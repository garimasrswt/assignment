import React, { Component } from "react";
import './slideDrawer.css';

export default class SlideDrawer extends Component{
 
    render(){
        const {student} = this.props;
        let drawerClasses = 'side-drawer'
        if(this.props.show) {
           drawerClasses = 'side-drawer open'
        }
        return(
            <div className={drawerClasses}>
                <div onClick= {this.props.close} className="close_btn">X</div>
                <div>Name {student.name}</div>
                <div>Age {student.age}</div>
                <div>Gender {student.gender}</div>
                <div><span>Sports </span>{" "} <span> {student.sports?.map((sport, index)=>(
                    <span key={index}>{` ${sport} `}</span>
                ))}</span></div>
            </div>
        )
    }
}