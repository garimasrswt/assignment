import React, { Component } from "react";
import './tooltip.css'

export default class Tooltip extends Component {
  render() {
      const {content,position} = this.props;
    return (
      <div
        id="tooltip"
        className="right"
        style={{ left: `${position.left}px`, top: `${position.top}px` }}
      >
        <div className="tooltip-arrow"></div>
        <div className="tooltip-label">
            <div>Name: {content.name}</div>
            <div>Age:{content.age}</div>
            <div>Gender:{content.gender}</div>
            <div>Sports:{content.sports?.map((sport,index)=>(
                <span key={index}>{sport}</span>
            ))}</div>
        </div>
      </div>
    );
  }
}
