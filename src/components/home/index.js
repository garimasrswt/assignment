import React, { Component } from "react";
import ReactTooltip from "react-tooltip";
import axios from 'axios';

import "./home.css";
import SlideDrawer from "../slideDrawer";
import Backdrop from "../slideDrawer/backdrop";
import Tooltip from "../tooltip";
import { computeHeadingLevel } from "@testing-library/dom";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      students: [],
      classes: [],
      sections: [],
      showDrawer: false,
      selectedStudent: {},
      showTooltip: false,
      position:{left:0,top:0},
      left: 0
    };
  }

  async componentDidMount() {
   await axios.get('https://student-management-api-1u3cd4j7s.now.sh/students').then((result)=>{
        this.setState({students:result.data});
    }).catch(err=>{
        console.log(err)
    })  
    const classes = this.state.students.map((data) => {
      return data.class;
    });
    const uniqueClasses = classes.filter((val,id,array) => array.indexOf(val) == id);
    this.setState({ classes: uniqueClasses });
    const sections = this.state.students.map((data) => {
      return data.section;
    });
    const uniqueSections = sections.filter((val,id,array) => array.indexOf(val) == id)
    this.setState({ sections: uniqueSections });
  }

  toggleDrawer(e, student) {
    e.preventDefault();
    this.setState((prevState) => {
      return { ...this.state, showDrawer: !prevState.showDrawer };
    });
    this.setState({ selectedStudent: student });
  }

  toolTipContent(e, student, index) {
    e.preventDefault();
    this.setState((prevState) =>{
        let position = Object.assign({}, prevState.position);  
        position.top = e.clientY;
        position.left= e.clientX;                                  
        return { position }; 
    },() =>{
        this.setState({ selectedStudent: student }, () => {
            this.setState((prevState) => {
              return { ...this.state, showTooltip: !prevState.showTooltip };
            });
          });
    });

  }

  render() {
    const {
      classes,
      sections,
      students,
      showDrawer,
      selectedStudent,
      showTooltip,
    } = this.state;
    let backdrop;
    if (this.state.showDrawer) {
      backdrop = <Backdrop close={(e) => this.toggleDrawer(e)} />;
    }
    return (
      <div className="main_container">
        <ul>
          {classes.map((clas) => (
            <li key={clas}>
              Class {clas}
              <ul>
                {sections.map((section, index) => (
                  <li key={index}>
                    Section {section}
                    <ul className="student_list">
                      {students
                        .filter((student) => {
                          return (
                            student.class === clas &&
                            student.section === section
                          );
                        })
                        .map((student, index) => {
                          return (
                            <span
                              onClick={(e) => this.toggleDrawer(e, student)}
                              key={index}
                              onMouseEnter={(e) =>
                                this.toolTipContent(e, student, index)
                              }
                              onMouseOut={(e) =>
                                this.toolTipContent(e, student, index)
                              }
                            >
                              {" "}
                              {student.name}
                              {showTooltip && (
                                <Tooltip content={selectedStudent} position={this.state.position}/>
                              )}
                            </span>
                          );
                        })}
                    </ul>
                  </li>
                ))}
              </ul>
            </li>
          ))}
        </ul>
        {showDrawer && (
          <SlideDrawer
            show={showDrawer}
            student={selectedStudent}
            close={(e) => this.toggleDrawer(e)}
          />
        )}
        {backdrop}
      </div>
    );
  }
}
